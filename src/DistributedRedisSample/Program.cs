﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace DistributedRedisSample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DisRedis().Wait();
            Console.WriteLine("ok.");
            Console.ReadLine();
        }

        private static async Task DisRedis()
        {
            var mux = await ConnectionMultiplexer.ConnectAsync(new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                AllowAdmin = true,
                EndPoints = { "127.0.0.1:7003" }
            });
            var db = mux.GetDatabase();
            var test = db.StringGet("redistest");
            Console.WriteLine(test);
            db.StringSet("redistest", "1234235");
            test = db.StringGet("redistest");
            Console.WriteLine(test);
        }
    }
}
